sudo apt-get install vagrant
sudo apt-get install virtualbox
sudo apt-get install python-setuptools
sudo pip install fabtools
vagrant box add precise32 http://files.vagrantup.com/precise32.box
git clone https://CGII@bitbucket.org/b-soft/openerp7_vagrant.git or git clone https://CGII@bitbucket.org/b-soft/openerp8_vagrant.git
vagrant up
vagrant ssh

If error while browsing or creating the db, do:
##################openerp7 DataError: encoding UTF8 does not match locale en_US######################

I had the same problem. The problem is connected with wrong encoding in PostgreSQL and template1 database. The solution is to fix encoding in Linux to UTF-8 and then:

sudo su postgres

psql

update pg_database set datistemplate=false where datname='template1';
drop database Template1;
create database template1 with owner=postgres encoding='UTF-8'

lc_collate=len_US.utf8' lc_ctype='en_US.utf8' template template0;

update pg_database set datistemplate=true where datname='template1';
################################################################################

