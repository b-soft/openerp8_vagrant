sudo apt-get install vagrant
sudo apt-get install virtualbox
$ mkdir vagrant_getting_started
$ cd vagrant_getting_started
$ vagrant init
$ vagrant box add <name> <url> [--provider provider] [-h] <url> =  http://files.vagrantup.com/precise32.box
 e.g: vagrant box add precise32 http://files.vagrantup.com/precise32.box (Many boxes available at: https://atlas.hashicorp.com/boxes/search or http://files.vagrantup.com/)
$ vagrant up
$ vagrant ssh

Installing Apache

We'll just setup Apache for our basic project, and we'll do so using a shell script. Create the following shell script and save it as bootstrap.sh in the same directory as your Vagrantfile:

#!/usr/bin/env bash

apt-get update
apt-get install -y apache2
if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant /var/www
fi

Next, we configure Vagrant to run this shell script when setting up our machine. We do this by editing the Vagrantfile, which should now look like this:

Vagrant.configure("2") do |config|
  config.vm.box = "hashicorp/precise32"
  config.vm.provision :shell, path: "bootstrap.sh"
end

Port Forwarding

One option is to use port forwarding. Port forwarding allows you to specify ports on the guest machine to share via a port on the host machine. This allows you to access a port on your own machine, but actually have all the network traffic forwarded to a specific port on the guest machine.

Let's setup a forwarded port so we can access Apache in our guest. Doing so is a simple edit to the Vagrantfile, which now looks like this:

Vagrant.configure("2") do |config|
  config.vm.box = "hashicorp/precise32"
  config.vm.provision :shell, path: "bootstrap.sh"
  config.vm.network :forwarded_port, host: 4567, guest: 80
end

Run a vagrant reload or vagrant up (depending on if the machine is already running) so that these changes can take effect.

Once the machine is running again, load http://127.0.0.1:4567 in your browser. You should see a web page that is being served from the virtual machine that was automatically setup by Vagrant.

Share

Now that we have a web server up and running and accessible from your machine, we have a fairly functional development environment. But in addition to providing a development environment, Vagrant also makes it easy to share and collaborate on these environments. The primary feature to do this in Vagrant is called Vagrant Share.

Vagrant Share lets you share your Vagrant environment to anyone around the world. It will give you a URL that will route directly to your Vagrant environment from any device in the world that is connected to the internet.
Login to HashiCorp's Atlas

Before being able to share your Vagrant environment, you'll need an account on HashiCorp's Atlas. Don't worry, it's free.

Once you have an account, log in using vagrant login:

$ vagrant login
Username or Email: mitchellh
Password (will be hidden):
You're now logged in!

Share

Once you're logged in, run vagrant share:

$ vagrant share
...
==> default: Your Vagrant Share is running!
==> default: URL: http://frosty-weasel-0857.vagrantshare.com
...

Your URL will be different, so don't try the URL above. Instead, copy the URL that vagrant share outputted for you and visit that in a web browser. It should load the index page we setup in the previous pages.

Now, modify your "index.html" file and refresh the URL. It will be updated! That URL is routing directly into your Vagrant environment, and works from any device in the world that is connected to the internet.

To end the sharing session, hit Ctrl+C in your terminal. You can refresh the URL again to verify that your environment is no longer being shared.

Vagrant Share is much more powerful than simply HTTP sharing. For more details, see the complete Vagrant Share documentation.


